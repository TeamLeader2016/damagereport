package DMGReport.view;

import DMGReport.controller.DataController;
import DMGReport.controller.ExceptionHandler;
import DMGReport.model.Damage;
import DMGReport.model.Room;
import DMGReport.model.User;

import javax.swing.*;
import java.util.List;
import java.util.Objects;

public class GUI {
    /**
     * Function to display the GUI for entering a Password
     */
    private static String showPasswordGUI() {
        String retString = "";

        JPasswordField passwordField = new JPasswordField();
        passwordField.setEchoChar('*');
        Object[] obj = {"Please enter the password:\n\n", passwordField};
        Object stringArray[] = {"OK", "Cancel"};
        if (JOptionPane.showOptionDialog(null, obj, "Passwort", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray, obj) == JOptionPane.YES_OPTION)
            retString = new String(passwordField.getPassword());
        DataController.logger.info("Passwort wurde eingegeben.");
        return retString;
    }

    /**
     * This function allow the registered user to login
     * if the password is  wrong you get a notification and go back to the main menu
     */
    private static void loginGUI() {
        String eingabe = "";
        int matricNumber = 0;
        String password = "";

        do {
            eingabe = JOptionPane.showInputDialog(null, "Bitte Matrikelnummer eingeben", "1234567890");
            DataController.logger.info("Matrikelnummer wurde eingegeben.");
            matricNumber = ExceptionHandler.tryParseInt(eingabe);
            password = showPasswordGUI();
            if (eingabe.equals("") || password.equals(""))
                JOptionPane.showMessageDialog(null, "Mindestens eine Eingabe war leer");
        } while (eingabe.equals("") || password.equals(""));

        DataController.loginUser(matricNumber, password);
        if (DataController.currentUser == null) {
            JOptionPane.showMessageDialog(null, "Matrikelnummer oder Passwort falsch");
        } else {
            JOptionPane.showMessageDialog(null, "Hallo " + DataController.currentUser.getFirstName());
        }

    }


    private static void loginAsAdminGUI() {

        String eingabe = "";
        int AdminNumber = 0;
        String password = "";

        do {
            eingabe = JOptionPane.showInputDialog(null, "Ihre Admin Nummer Eingeben", "1234567890");
            DataController.logger.info("Admin Nummer wurde eingegeben.");
            AdminNumber = ExceptionHandler.tryParseInt(eingabe);
            password = showPasswordGUI();
            if (eingabe.equals("") || password.equals(""))
                JOptionPane.showMessageDialog(null, "Mindestens eine Eingabe war leer");
        } while (eingabe.equals("") || password.equals(""));


    }


    /**
     * This function let the user start his registration
     * user have to put in matric number, password, name, gender, birthday, secret question/answer
     */
    private static void registerGUI() {
        String eingabe = "";
        int matricNumber = 0;
        String password = "";
        String firstName = "";
        String lastName = "";
        String genders[] = {"Männlich", "Weiblich"};
        String gender = "";
        String birthday = "";
        String secretQuestios[] = {"Wie hieß Ihr erstes Haustier ?", "In welcher Straße sind sie aufgewachsen ?", "Eigene Frage erstellen"};
        String chosenQuestion = "";
        String secretAnswer = "";
        int points = 1000;

        eingabe = JOptionPane.showInputDialog(null, "Bitte Matrikelnummer eingeben", "1234567890");
        matricNumber = ExceptionHandler.tryParseInt(eingabe);

        password = showPasswordGUI();

        firstName = JOptionPane.showInputDialog("Bitte Vorname eingeben");
        lastName = JOptionPane.showInputDialog("Bitte Nachname eingeben");
        gender = (String) JOptionPane.showInputDialog(null, "Männlich oder Weiblich ?", "Geschlecht auswählen", JOptionPane.PLAIN_MESSAGE, null, genders, genders[0]);
        birthday = JOptionPane.showInputDialog(null, "Bitte Geburtsdatum eingeben", "YYYY-MM-DD");

        chosenQuestion = (String) JOptionPane.showInputDialog(null, "Bitte geheime Frage wählen", "Geheimfrage", JOptionPane.PLAIN_MESSAGE, null, secretQuestios, secretQuestios[0]);
        if (chosenQuestion.equals(secretQuestios[secretQuestios.length - 1])) {
            chosenQuestion = JOptionPane.showInputDialog(null, "Hier können Sie sich selbst eine Geheimfrage erstellen.");
        }
        secretAnswer = JOptionPane.showInputDialog(null, "Die Antwort auf Ihre Geheimfrage. Sie wird benötigt, falls Sie ihr Kenntwort vergessen haben.");

        if (DataController.findUser(matricNumber) != null) {
            JOptionPane.showMessageDialog(null, "Die von Ihnen eingegebene Matrikelnummer ist bereits registriert.");
            int YesNoOption = JOptionPane.showConfirmDialog(null, "Sind Sie bereits registriert aber haben Ihr Passwort vergessen ?", "Abfrage", JOptionPane.YES_NO_OPTION);
            if (YesNoOption == JOptionPane.YES_OPTION) {
                GUI.forgottenPasswordGUI();
            }
        }
        if (matricNumber != 0 && !password.equals("")) {
            DataController.currentUser = new User(firstName, lastName, gender, birthday, matricNumber, password, chosenQuestion, secretAnswer, points);
            DataController.allUsers.add(DataController.currentUser);
            JOptionPane.showMessageDialog(null, "Sie wurden erfolgreich registriert.");
            DataController.logger.info("User wurde erfolgreich registriert.");
        } else {
            JOptionPane.showMessageDialog(null, "Eingaben waren nicht korrekt.");
        }
    }

    /**
     * This function allow the user to report a damage
     * user have to put in damage- type(category),wise of damage, some extra information, description of damage and room- number
     */
    private static void reportDamageGUI() {
        Room room = null;
        String damageType[] = {"Beamer", "Sitz", "Tisch", "Sonstiges"};
        String categorie = (String) JOptionPane.showInputDialog(null, "Schaden wählen", "Schadensart", JOptionPane.PLAIN_MESSAGE, null, damageType, damageType[0]);
        if (Objects.equals(categorie, "Sonstiges")) {
            categorie = JOptionPane.showInputDialog("Bitte Schadensart eingeben");
        }
        String description = JOptionPane.showInputDialog("Beschreibung des Schadens");
        while (description.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Bitte geben Sie eine Art des Schadens an.");
            description = JOptionPane.showInputDialog("Beschreibung des Schadens");
        }
        String roomList[] = new String[DataController.allRooms.size()];
        for (
                int i = 0;
                i <= DataController.allRooms.size() - 1; i++)

        {
            roomList[i] = DataController.allRooms.get(i).getHouseNr() + "." + DataController.allRooms.get(i).getStage() + "." + DataController.allRooms.get(i).getRoomNr();
        }

        String roomChoose = (String) JOptionPane.showInputDialog(null, "Raum auswählen", "Raumauswahl", JOptionPane.PLAIN_MESSAGE, null, roomList, roomList[0]);
        for (
                int i = 0;
                i <= DataController.allRooms.size() - 1; i++)

        {
            if (roomChoose.equals(DataController.allRooms.get(i).getHouseNr() + "." + DataController.allRooms.get(i).getStage() + "." + DataController.allRooms.get(i).getRoomNr())) {
                room = DataController.allRooms.get(i);
            }
        }

        Damage currentDamage = new Damage(DataController.getDate(), categorie, description, room, DataController.currentUser);
        DataController.allDamages.add(currentDamage);


    }

    /**
     * Function to delete a damagereport from the text file.
     */
    private static void deleteDamageGUI() {
        Damage deleteDamage = null;
        List<Damage> tempReportedDamage = DataController.currentUser.getReportedDamages();
        String userDamageList[] = new String[DataController.currentUser.getReportedDamages().size()];
        if (DataController.currentUser.getReportedDamages().size() == 0) {
            JOptionPane.showMessageDialog(null, "Es wurden noch keine Schäden gemeldet");
        } else {
            for (int i = 0; i < DataController.currentUser.getReportedDamages().size(); i++) {
                userDamageList[i] = tempReportedDamage.get(i).getRoom().getHouseNr() + "." + tempReportedDamage.get(i).getRoom().getStage() + "." + tempReportedDamage.get(i).getRoom().getRoomNr() + "/" + tempReportedDamage.get(i).getDescription() + "/" + tempReportedDamage.get(i).getCategorie();
            }

            String choosedDamage = (String) JOptionPane.showInputDialog(null, "Schaden auswählen", "Schadensauflistung", JOptionPane.PLAIN_MESSAGE, null, userDamageList, userDamageList[0]);
            for (int i = 0; i <= tempReportedDamage.size() - 1; i++) {
                if (choosedDamage.equals(tempReportedDamage.get(i).getRoom().getHouseNr() + "." + tempReportedDamage.get(i).getRoom().getStage() + "." + tempReportedDamage.get(i).getRoom().getRoomNr() + "/" + tempReportedDamage.get(i).getDescription() + "/" + tempReportedDamage.get(i).getCategorie())) {

                    deleteDamage = DataController.currentUser.getReportedDamages().get(i);
                }
            }
            int result = JOptionPane.showConfirmDialog(null, "Möchten Sie den Schaden wirklich löschen?", null, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                deleteDamage.deleteDamage();
            } else {
                JOptionPane.showMessageDialog(null, "Schaden wurde nicht gelöscht.");
            }
        }
    }


    /**
     * Function to start the GUI.
     */
    public static void startGUI() {

        try {


            String regOrLogin[] = {"Registrieren", "Anmelden", "Passwort vergessen", "Als Admin Anmelden"};
            String regOrLoginUI = "";

            do {
                regOrLoginUI = (String) JOptionPane.showInputDialog(null, "Was möchten Sie tun ?", "Aktion", JOptionPane.QUESTION_MESSAGE, null, regOrLogin, regOrLogin[0]);
                if (Objects.equals(regOrLoginUI, "Anmelden")) {
                    GUI.loginGUI();
                } else if (Objects.equals(regOrLoginUI, "Registrieren")) {
                    GUI.registerGUI();
                } else if (Objects.equals(regOrLoginUI, "Passwort vergessen")) {
                    GUI.forgottenPasswordGUI();
                } else if (Objects.equals(regOrLoginUI, "Als Admin Anmelden")) {
                    GUI.loginAsAdminGUI();
                }
            } while (regOrLoginUI != null && DataController.currentUser == null);

            //cheken ob matrikel nummer (dann gibts 3 optionen! auswählen!)
            if (DataController.currentUser != null) {
                String reportOrCancel[] = {"Schaden melden", "Schaden löschen", "Abbrechen"};
                String reportOrCancelUI;
                do {
                    reportOrCancelUI = (String) JOptionPane.showInputDialog(null, "Was möchten Sie tun ?", "Aktion", JOptionPane.PLAIN_MESSAGE, null, reportOrCancel, reportOrCancel[0]);
                    if (reportOrCancelUI.equals("Schaden melden")) {
                        GUI.reportDamageGUI();
                    }
                    if (reportOrCancelUI.equals("Schaden löschen")) {
                        GUI.deleteDamageGUI();
                    }
                } while (!reportOrCancelUI.equals("Abbrechen"));
            }


        } catch (Exception e) {
            // e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Programm wird beendet");
        }
    }

    /**
     * Function to rebase your password if u forgot it
     * user have to answer his secret question, after he could set a new password , if the both passwords identical
     * it will be changed in the storage and the user got a new password
     */
    private static void forgottenPasswordGUI() {

        String matricNumberString = JOptionPane.showInputDialog(null, "Bitte Ihre Matrikelnummer eingeben.", JOptionPane.YES_OPTION);
        int matricNumber = ExceptionHandler.tryParseInt(matricNumberString);

        User userWithForgottenPassword = DataController.findUser(matricNumber);
        if (userWithForgottenPassword != null && userWithForgottenPassword.getMatricNumber() == matricNumber) {
            String secretAnswer = JOptionPane.showInputDialog(null, userWithForgottenPassword.getSecretQuestion(), "Bitte die Frage beantworten");
            if (secretAnswer.equals(userWithForgottenPassword.getSecretAnswer())) {
                String password1 = "";
                String password2 = "";
                do {
                    {
                        password1 = JOptionPane.showInputDialog(null, "Neues Passwort eingeben");
                        password2 = JOptionPane.showInputDialog(null, "Bitte Passwort wiederholen");
                    }
                } while (!password1.equals(password2));
                userWithForgottenPassword.setPassword(password1);
                JOptionPane.showMessageDialog(null, "Passwort wurde geändert.");
            } else {
                JOptionPane.showMessageDialog(null, "Geheimfrage wurde falsch beantwortet.");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Falsche Matrikelnummer");
        }

    }


    private static void deleteUserAsAdmin() {

    }

    private static void doneWorkAsAdmin() {

    }

    private static void deleteDamageAsAdmin() {

    }
}

