package DMGReport;

import DMGReport.controller.DataController;
import DMGReport.model.Damage;
import DMGReport.model.Room;
import DMGReport.model.User;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {

        DataController DC = new DataController();
        DC.loadData();


        User test_user = new User("Andre", "Schimmoch", "Männlich", "1996-08-15", 996088, "passwort", "KeineFrage", "KeineAntwort", 1000);
        Room test_room = new Room(1, 1, 1);
        Damage test_dmg = new Damage(DC.getDate(), "Buch", "Test", test_room, test_user);

        DC.allUsers.add(test_user);
        DC.allDamages.add(test_dmg);
        DC.allRooms.add(test_room);


        DC.saveData();


    }
}

