package DMGReport.controller;

import DMGReport.model.Damage;
import DMGReport.model.Room;
import DMGReport.model.User;

import java.sql.*;
import java.util.Date;


public class DatabaseController {

    private static final String TABLE_USERS_NAME = "users";
    private static final String TABLE_USERS_COLUMN_MATRIC_NUMBER = "matric_number";
    private static final String TABLE_USERS_COLUMN_FIRSTNAME = "firstname";
    private static final String TABLE_USERS_COLUMN_LASTNAME = "lastname";
    private static final String TABLE_USERS_COLUMN_GENDER = "gender";
    private static final String TABLE_USERS_COLUMN_PASSWORD = "password";
    private static final String TABLE_USERS_COLUMN_SECRET_ANSWER = "secret_answer";
    private static final String TABLE_USERS_COLUMN_SECRET_QUESTION = "secret_question";
    private static final String TABLE_USERS_COLUMN_BIRTHDAY = "birthday";
    private static final String TABLE_USERS_COLUMN_POINTS = "points";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TABLE_DAMAGES_NAME = "damages";
    private static final String TABLE_DAMAGES_COLUMN_CATEGORY = "category";
    private static final String TABLE_DAMAGES_COLUMN_DAMAGE_ID = "damage_id";
    private static final String TABLE_DAMAGES_COLUMN_DATE = "date";
    private static final String TABLE_DAMAGES_COLUMN_DESCRIPTION = "description";
    private static final String TABLE_DAMAGES_COLUMN_MATRIC_NUMBER = "matric_number";
    private static final String TABLE_DAMAGES_COLUMN_ROOM_ID = "room_id";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static final String TABLE_ROOMS_NAME = "rooms";
    private static final String TABLE_ROOMS_COLUMN_ROOM_ID = "room_id";
    private static final String TABLE_ROOMS_COLUMN_ROOM_NUMBER = "room_number";
    private static final String TABLE_ROOMS_COLUMN_HOUSE_NUMBER = "house_number";
    private static final String TABLE_ROOMS_COLUMN_STAGE_NUMBER = "stage_number";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////// INSERTS //////////////////////////////
    private static final String INSERT_USER = "INSERT INTO " + TABLE_USERS_NAME + " VALUES (?,?,?,?,?,?,?,?,?)";
    private static final String INSERT_DAMAGE = "INSERT INTO " + TABLE_DAMAGES_NAME + " VALUES (?,?,?,?,?,?)";

    ////////////////////////////// UPDATES //////////////////////////////
    private static final String UPDATE_USER = "UPDATE " + TABLE_USERS_NAME + " SET  " +
            TABLE_USERS_COLUMN_FIRSTNAME + "=?," +
            TABLE_USERS_COLUMN_LASTNAME + "=?," +
            TABLE_USERS_COLUMN_BIRTHDAY + "=?," +
            TABLE_USERS_COLUMN_GENDER + "=?," +
            TABLE_USERS_COLUMN_PASSWORD + "=?," +
            TABLE_USERS_COLUMN_SECRET_ANSWER + "=?," +
            TABLE_USERS_COLUMN_SECRET_QUESTION + "=? " +
            TABLE_USERS_COLUMN_POINTS + "=? " +
            "WHERE " + TABLE_USERS_COLUMN_MATRIC_NUMBER + " = ?";

    private static final String UPDATE_DAMAGE = "UPDATE " + TABLE_DAMAGES_NAME + " SET  " +
            TABLE_DAMAGES_COLUMN_MATRIC_NUMBER + "=?," +
            TABLE_DAMAGES_COLUMN_DATE + "=?," +
            TABLE_DAMAGES_COLUMN_CATEGORY + "=?," +
            TABLE_DAMAGES_COLUMN_DESCRIPTION + "=?," +
            TABLE_DAMAGES_COLUMN_ROOM_ID + "=? " +
            "WHERE " + TABLE_DAMAGES_COLUMN_DAMAGE_ID + " = ?";

    private static final String UPDATE_ROOM = "UPDATE " + TABLE_ROOMS_NAME + " SET  " +
            TABLE_ROOMS_COLUMN_ROOM_ID + "=?," +
            TABLE_ROOMS_COLUMN_ROOM_NUMBER + "=?," +
            TABLE_ROOMS_COLUMN_HOUSE_NUMBER + "=?," +
            TABLE_ROOMS_COLUMN_STAGE_NUMBER + "=? " +
            "WHERE " + TABLE_ROOMS_COLUMN_ROOM_ID + " = ?";


    ////////////////////////////// SELECTS //////////////////////////////
    private static final String SELECT_USERS = "SELECT * FROM " + TABLE_USERS_NAME;
    private static final String SELECT_ROOMS = "SELECT * FROM " + TABLE_ROOMS_NAME;
    private static final String SELECT_DAMAGES = "SELECT * FROM " + TABLE_DAMAGES_NAME;


    private Connection con;
    private PreparedStatement insertUserStatement;
    private PreparedStatement insertDamageStatement;
    private PreparedStatement insertRoomStatement;

    private PreparedStatement loadDamagesStatement;
    private PreparedStatement loadUsersStatement;
    private PreparedStatement loadRoomsStatement;

    private PreparedStatement updateUserStatement;
    private PreparedStatement updateDamageStatement;
    private PreparedStatement updateRoomStatement;


    public DatabaseController() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dmgreport", "root", "");

            this.insertUserStatement = this.con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            this.insertDamageStatement = this.con.prepareStatement(INSERT_DAMAGE, Statement.RETURN_GENERATED_KEYS);
            this.loadDamagesStatement = this.con.prepareStatement(INSERT_DAMAGE, Statement.RETURN_GENERATED_KEYS);
            this.loadUsersStatement = this.con.prepareStatement(SELECT_USERS, Statement.RETURN_GENERATED_KEYS);
            this.loadRoomsStatement = this.con.prepareStatement(SELECT_ROOMS, Statement.RETURN_GENERATED_KEYS);

            this.updateUserStatement = this.con.prepareStatement(UPDATE_USER, Statement.RETURN_GENERATED_KEYS);

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }


    }

    public void createDatabase() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306", "root", "");
            PreparedStatement createDatabase = this.con.prepareStatement("CREATE DATABASE dmgreport", Statement.RETURN_GENERATED_KEYS);
            createDatabase.execute();
            System.out.println("Datenbank erfolgreich erstellt");
        } catch (Exception e) {
            System.out.println("Datenbank konnte nicht erstellt werden");
            System.out.println("Error:" + e);

        }
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/dmgreport", "root", "");
        } catch (Exception e) {
            System.out.println("Error:" + e);
        }

        createTableDamages();
        createTableRooms();
        createTableUsers();
        setPrimaryKeysInTables();

    }

    private void createTableUsers() {
        String createTableUsers = "CREATE TABLE `users` (" +
                "  `matric_number` int(11) NOT NULL," +
                "  `firstname` varchar(255) NOT NULL," +
                "  `lastname` varchar(255) NOT NULL," +
                "  `gender` varchar(255) NOT NULL," +
                "  `birthday` date NOT NULL," +
                "  `password` varchar(255) NOT NULL," +
                "  `secret_question` varchar(255) NOT NULL," +
                "  `secret_answer` varchar(255) NOT NULL" +
                "  `points` decimal(7.2) NOT NULL)";

        try {
            PreparedStatement createDatabase = this.con.prepareStatement(createTableUsers, Statement.RETURN_GENERATED_KEYS);
            createDatabase.execute();
            System.out.println("Tabelle Users wurde erstellt");
        } catch (Exception e) {
            System.out.println("Die Tabelle users konnte nicht erstellt werden");
        }

    }

    private void createTableDamages() {
        String createTableDamages = "CREATE TABLE `damages` (" +
                "  `damage_id` int(11) NOT NULL," +
                "  `matric_number` int(11) NOT NULL," +
                "  `date` date NOT NULL," +
                "  `category` varchar(255) NOT NULL DEFAULT '0'," +
                "  `description` varchar(255) NOT NULL," +
                "  `room_id` int(11) NOT NULL)";
        try {
            PreparedStatement createDatabase = this.con.prepareStatement(createTableDamages, Statement.RETURN_GENERATED_KEYS);
            createDatabase.execute();
            System.out.println("Tabelle Damages wurde erstellt");
        } catch (Exception e) {
            System.out.println("Die Tabelle damages konnte nicht erstellt werden");

        }

    }

    private void createTableRooms() {
        String createTableRooms = "CREATE TABLE `rooms` (" +
                "  `room_id` int(11) NOT NULL," +
                "  `room_number` int(11) NOT NULL," +
                "  `house_number` int(11) NOT NULL," +
                "  `stage_number` int(11) NOT NULL)";
        try {
            PreparedStatement createDatabase = this.con.prepareStatement(createTableRooms, Statement.RETURN_GENERATED_KEYS);
            createDatabase.execute();
            System.out.println("Tabelle rooms wurde erstellt");
        } catch (Exception e) {
            System.out.println("Die Tabelle rooms konnte nicht erstellt werden");
        }

    }

    private void setPrimaryKeysInTables() {
        String alterDamages = "ALTER TABLE `damages`" +
                "  ADD PRIMARY KEY (`damage_id`);";

        String alterRooms = "ALTER TABLE `rooms`" +
                "  ADD PRIMARY KEY (`room_id`);";

        String alterUsers = "ALTER TABLE `users`" +
                "  ADD PRIMARY KEY (`matric_number`);";


        try {
            PreparedStatement PrepareAlterDamages = this.con.prepareStatement(alterDamages, Statement.RETURN_GENERATED_KEYS);
            PreparedStatement PrepareAlterRooms = this.con.prepareStatement(alterRooms, Statement.RETURN_GENERATED_KEYS);
            PreparedStatement PrepareAlterUsers = this.con.prepareStatement(alterUsers, Statement.RETURN_GENERATED_KEYS);

            PrepareAlterDamages.executeUpdate();
            PrepareAlterRooms.executeUpdate();
            PrepareAlterUsers.executeUpdate();

        } catch (Exception e) {
            System.out.println("Die Tabellen konnten nicht modifiziert werden(Primary Keys setzen)");
            System.out.println("Error: " + e);
        }
    }

    /*
     * Function to save all Users of the allUsers-List into the Database
     */
    public void saveUserList() {
        for (int i = 0; i < DataController.allUsers.size(); i++) {
            DataController.logger.info(DataController.allUsers.get(i).getFirstName() + " wird jetzt in saveOrUpdateUser geworfen");
            saveOrUpdateUser(DataController.allUsers.get(i));
        }
    }

    public void saveDamageList() {
        for (int i = 0; i < DataController.allDamages.size(); i++) {
            DataController.logger.info(DataController.allDamages.get(i).getId() + " wird jetzt in saveOrUpdateDamage geworfen");
            saveOrUpdateDamage(DataController.allDamages.get(i));
        }
    }

    /*
     * Function to save one single User into the Database
     */
    private void saveUser(User user) {
        try {
            this.insertUserStatement.setString(1, Integer.toString(user.getMatricNumber()));
            this.insertUserStatement.setString(2, user.getFirstName());
            this.insertUserStatement.setString(3, user.getLastName());
            this.insertUserStatement.setString(4, user.getGender());
            this.insertUserStatement.setString(5, user.getBirthday());
            this.insertUserStatement.setString(6, user.getPassword());
            this.insertUserStatement.setString(7, user.getSecretQuestion());
            this.insertUserStatement.setString(8, user.getSecretAnswer());
            this.insertUserStatement.setString(9, Integer.toString(user.getPoints()));
            DataController.logger.info(user.getFirstName() + " " + user.getLastName() + " wird gespeichert.");
            this.insertUserStatement.execute();

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        DataController.logger.info(user.getFirstName() + " wurde gepspeichert.");
    }

    private void saveDamage(Damage damage) {
        try {
            this.insertDamageStatement.setInt(1, damage.getId());
            this.insertDamageStatement.setString(2, Integer.toString(damage.getUser().getMatricNumber()));
            this.insertDamageStatement.setString(3, damage.getDate().toString());
            this.insertDamageStatement.setString(4, damage.getCategorie());
            this.insertDamageStatement.setString(5, damage.getDescription());
            this.insertDamageStatement.setInt(6, damage.getRoom().getId());

            //String temp = this.insertDamageStatement.toString();
            //JOptionPane.showMessageDialog(null, temp);

            this.insertDamageStatement.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * Function to load all Users from the Database into the allUsers-List
     */
    public void loadUsers() {
        try {
            String sql = "SELECT * FROM " + TABLE_USERS_NAME;
            PreparedStatement selectFromUsers = this.con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = selectFromUsers.executeQuery();

            while (rs.next()) {
                //Retrieve by column name
                int matric_Number = rs.getInt(TABLE_USERS_COLUMN_MATRIC_NUMBER);
                String firstName = rs.getString(TABLE_USERS_COLUMN_FIRSTNAME);
                String lastName = rs.getString(TABLE_USERS_COLUMN_LASTNAME);
                String gender = rs.getString(TABLE_USERS_COLUMN_GENDER);
                Date birthday = rs.getDate(TABLE_USERS_COLUMN_BIRTHDAY);
                String password = rs.getString(TABLE_USERS_COLUMN_PASSWORD);
                String secretQuestion = rs.getString(TABLE_USERS_COLUMN_SECRET_QUESTION);
                String secretAnswer = rs.getString(TABLE_USERS_COLUMN_SECRET_ANSWER);
                int points = rs.getInt(TABLE_USERS_COLUMN_POINTS);

                User tempUser = new User(firstName, lastName, gender, birthday.toString(), matric_Number, password, secretQuestion, secretAnswer, points);
                DataController.allUsers.add(tempUser);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        DataController.logger.info("User wurden geladen.");
    }

    public void loadRooms() {
        try {
            String sql = "SELECT * FROM " + TABLE_ROOMS_NAME;
            PreparedStatement selectFromRooms = this.con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = selectFromRooms.executeQuery();

            while (rs.next()) {
                //Retrieve by column name
                int room_number = rs.getInt("room_number");
                int house_number = rs.getInt("house_number");
                int stage_number = rs.getInt("stage_number");

                Room tempRoom = new Room(room_number, house_number, stage_number);
                DataController.allRooms.add(tempRoom);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        DataController.logger.info("Räume wurden geladen.");

    }

    public void loadDamages() {
        try {
            String sql = "SELECT * FROM " + TABLE_DAMAGES_NAME;
            PreparedStatement selectFromDamages = this.con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = selectFromDamages.executeQuery();

            while (rs.next()) {
                //Retrieve by column name
                int damage_id = rs.getInt("damage_id");
                int matric_number = rs.getInt("matric_number");
                Date date = rs.getDate("date");
                String category = rs.getString("category");
                String description = rs.getString("description");
                int room_id = rs.getInt("room_id");
// TODO: Wieso ist das da drunter auskommentiert? Im Moment geht das Programm weil keine vorherigen Damages geladen werden, aber für die Zukunft brauchen wir die Zeile doch?
                //Damage tempDamage = new Damage(damage_id, matric_number, date, category_id,category_custom_id ,description ,room_id);
                //DataController.allDamages.add(tempDamage);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        DataController.logger.info("Schäden wurden geladen.");
    }

    private void updateUser(User user) {
        try {

            this.updateUserStatement = this.con.prepareStatement(UPDATE_USER, Statement.RETURN_GENERATED_KEYS);

            updateUserStatement.setString(1, user.getFirstName());
            updateUserStatement.setString(2, user.getLastName());
            updateUserStatement.setString(3, user.getBirthday().toString());
            updateUserStatement.setString(4, user.getGender());
            updateUserStatement.setString(5, user.getPassword());
            updateUserStatement.setString(6, user.getSecretAnswer());
            updateUserStatement.setString(7, user.getSecretQuestion());
            //updateUserStatement.setString(8, Integer.toString(user.getPoints()));
            //updateUserStatement.setString(9, Integer.toString(user.getMatricNumber()));
            //TODO: FEHLER bin tired mach ich morgen!
            updateUserStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DataController.logger.info(user.getFirstName() + " wurde geupdated.");
    }

    private void updateDamage(Damage damage) {
        try {
            Date datum = DataController.getDate();
            this.updateDamageStatement = this.con.prepareStatement(UPDATE_DAMAGE, Statement.RETURN_GENERATED_KEYS);

            updateDamageStatement.setInt(1, damage.getUser().getMatricNumber());
            updateDamageStatement.setDate(2, damage.getDate());
            updateDamageStatement.setString(3, damage.getCategorie());
            updateDamageStatement.setString(4, damage.getDescription());
            updateDamageStatement.setInt(5, damage.getRoom().getId());
            updateDamageStatement.setInt(6, damage.getId());
            System.out.println(updateDamageStatement);
            updateDamageStatement.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DataController.logger.info("Der Schaden mit der ID: " + damage.getId() + " wurde geupdated.");
    }

    private void saveOrUpdateUser(User user) {
        try {
            String sql = "SELECT * FROM " + TABLE_USERS_NAME + " WHERE " + TABLE_USERS_COLUMN_MATRIC_NUMBER + "=" + user.getMatricNumber();
            PreparedStatement selectFromUser = this.con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = selectFromUser.executeQuery();
            if (rs.first()) {
                updateUser(user);
            } else {
                saveUser(user);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    private void saveOrUpdateDamage(Damage damage) {
        try {
            String sql = "SELECT * FROM " + TABLE_DAMAGES_NAME + " WHERE " + TABLE_DAMAGES_COLUMN_DAMAGE_ID + "=" + damage.getId();
            PreparedStatement selectFromDamage = this.con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = selectFromDamage.executeQuery();
            if (rs.first()) {
                DataController.logger.info("Damage wird geupdated");
                updateDamage(damage);
            } else {
                DataController.logger.info("Damage wird neu angelegt");
                saveDamage(damage);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }


}
//TODO: String überprüfen des Datums //ODER direkt Datum einbauen