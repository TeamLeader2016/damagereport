package DMGReport.controller;

/**
 * Created by Malte on 25.04.2016.
 */
public class ExceptionHandler {

    /**
     * This is a method of the FileWriter class.
     *
     * @param string
     */

    public static int tryParseInt(String string) {
        int retInt = 0;
        try {
            retInt = Integer.parseInt(string);
            DataController.logger.info("String wurde in Integer umgewandelt");
        } catch (NumberFormatException e) {
            DataController.logger.warning(e.getLocalizedMessage());
            //e.printStackTrace();
            DataController.logger.info("String konnte nicht in Integer umgewandelt werden");
        }
        return retInt;

    }
}
