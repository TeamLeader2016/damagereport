package DMGReport.controller;

import DMGReport.model.Damage;
import DMGReport.model.Room;
import DMGReport.model.User;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.*;



public class DataController {

    public static User currentUser;
    public static List<User> allUsers;
    public static List<Room> allRooms;
    public static List<Damage> allDamages;
    public static DatabaseController DBC;
    public static int AdminM = 11111;
    public static Damage damage;

    public static Logger logger;

    /**
     * to build the Instances
     */
    public DataController() {
        currentUser = null;
        allUsers = new ArrayList<User>();
        allRooms = new ArrayList<Room>();
        allDamages = new ArrayList<Damage>();
        DBC = new DatabaseController();
        initializeLogger(Level.INFO);
    }

    /**
     * Function to load all rooms, users and damages
     *
     * @throws IOException
     */
    public static void loadData() throws IOException {

        DBC.loadUsers();
        DBC.loadDamages();
        DBC.loadRooms();

    }

    public static void saveData() throws IOException {
        DBC.saveUserList();
        DBC.saveDamageList();

    }

    /**
     * function to get the current date
     * @return current date
     */
    public static Date getDate() {

/*
 *  Declaring the calender.
 */
        GregorianCalendar now = new GregorianCalendar();
        DateFormat today = DateFormat.getDateInstance(DateFormat.SHORT);
        java.util.Date utilDate = new java.util.Date();
        Date sqlDate = new java.sql.Date(utilDate.getTime());
        return sqlDate;
    }

    /**
     * Function to initialize the logger. if there no logger you get an exception
     * @param level  information
     */
    public static void initializeLogger(Level level) {
        /*
         *  Configuring the logger
         */
        logger = Logger.getLogger("My Logger");
        try {
            Handler handler = new FileHandler("messages.log");
            handler.setFormatter(new SimpleFormatter());
            logger.addHandler(handler);
        } catch (IOException e) {
            logger.severe("Konnte keinen FileHandler zum Logger hinzufügen");
            e.printStackTrace();
        }
        logger.setLevel(level);
        logger.info("Logger erfolgreich konfiguriert");

    }

    /**
     * Function to find user in the array list
     * @param matricNumber primary key search
     * @return null
     */
    public static User findUser(int matricNumber) {
        for (User User : allUsers) {
            if (User.getMatricNumber() == matricNumber) {
                DataController.logger.info("User wurde gefunden.");
                return User;
            }
        }
        DataController.logger.info("Kein User gefunden");
        return null;
    }

    /**
     * Funtion to login user
     * @param matricNumber
     * @param password
     * @return false if one of the param. is false
     */
    public static boolean loginUser(int matricNumber, String password) {
        User tempUser = findUser(matricNumber);
        if (tempUser != null) {
            if (tempUser.getMatricNumber() == matricNumber && tempUser.getPassword().equals(password)) {
                if (tempUser.getMatricNumber() == AdminM) {
                    tempUser.setAdminStatus(User.SetStatusEnum.Admin);
                } else {
                    tempUser.setAdminStatus(User.SetStatusEnum.User);

                }

                currentUser = tempUser;
                logger.info(currentUser.getFirstName() + currentUser.getLastName() + " wurde eingeloggt.");
                return true;


            }
        }

        return false;
    }

    public static Damage findDamage(String Category) {
        for (Damage Damage : allDamages) {
            if (Damage.getCategorie() == Category) {

                return Damage;
            }
        }

        return null;
    }

    public static Damage showAllDamagesToAdmin() {


        if (currentUser.getMatricNumber() == AdminM) {
            allDamages.getClass();
            return damage;

        } else {
            return null;
        }

    }


    /**
     * Fuction to add points if a report was closed
     */
    public static void AddPointsToUser() {
        int DamageValue = 50;
        currentUser.setPoints(currentUser.getPoints() + DamageValue);
    }

    ;

/**
 public static boolean loginAdmin(int AdminNumber, String password) {


 User tempAdmin = findUser(AdminNumber);
 if (tempAdmin != null & AdminNumber == 11111) {
 if (tempAdmin.getMatricNumber() == AdminNumber && tempAdmin.getPassword().equals(password)) {
 currentUser = tempAdmin;
 logger.info(currentUser.getFirstName() + currentUser.getLastName() + " wurde eingeloggt.");
 return true;
 }
 }

 return false;
 }
 **/
}