package DMGReport.model;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Malte
 */

/**
 * Class "DMGReport.User" contains all the information about one user.
 */
public class User extends DatabaseEntitity {
    private List<Damage> reportedDamages = new ArrayList<>();
    private String firstName;
    ;
    private String lastName;
    private String gender;
    private String birthday;
    private int matricNumber;
    private String password;
    private String secretQuestion;
    private String secretAnswer;
    private SetStatusEnum Status = SetStatusEnum.User;
    private int points;
    /**
     * Registers the information about the user.
     *
     * @param gender, birthday, matricNumber, pass  Parameters to fill in the information.
     */

    public User(String firstName, String lastName, String gender, String birthday, int matricNumber, String pass, String question, String answer, int points) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
        this.matricNumber = matricNumber;
        this.password = pass;
        this.secretQuestion = question;
        this.secretAnswer = answer;
        this.points = points;
    }
    public User() {

    }

    /**
     * Setter and Getter of the objects.
     */

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getMatricNumber() {
        return matricNumber;
    }

    public void setMatricNumber(int matricNumber) {
        this.matricNumber = matricNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<Damage> getReportedDamages() {
        return reportedDamages;
    }

    /**
     * Method to add a new damage in the "storage" with the help of the "add" method of the "List" class.
     *
     * @param newDamage Parameter to define what kind of damage you want to add.
     */

    public void addNewDamage(Damage newDamage) {
        reportedDamages.add(newDamage);
    }

    /**
     * A method to get everything in our "storage" reported from this DMGReport.User
     */

    public void listReportedDmgFromUser() {
        int i = 1;
        for (Damage ausgabe : reportedDamages) {
            System.out.println(i + ". Schaden");
            System.out.println(ausgabe.getDate());
            System.out.println(ausgabe.getRoom());
            System.out.println(ausgabe.getUser());
            System.out.println(ausgabe.getCategorie());
            System.out.println(ausgabe.getDescription());
            i++;
        }
    }

    /**
     * A method to remove a specific damage from the list of the user.
     *
     * @param removeDamage Parameter to define what damage should be removed.
     */

    public void removeDamageFromList(Damage removeDamage) {

        this.reportedDamages.remove(reportedDamages.indexOf(removeDamage));
    }

    public SetStatusEnum getStatus() {
        return Status;

    }

    public void setAdminStatus(SetStatusEnum Status) {
        this.Status = Status;


    }

    public enum SetStatusEnum {Admin, User}

}
