package DMGReport.model;

import DMGReport.controller.DataController;

import java.sql.Date;

/**
 * @author Malte
 */

/**
 * Class "DMGReport.Damage" contains all information about the damage occurred.
 */

public class Damage extends DatabaseEntitity {
    private Date date;
    private String categorie;
    ;
    private String description;
    private Room room;
    private User user;
    private DamageStatus Status = DamageStatus.not_done;
    /**
     * Registers the information about the damage to work with.
     *
     * @param date date, String categorie, String description, DMGReport.Room room, DMGReport.User user  Parameters to fill in the information.
     */

    public Damage(Date date, String categorie, String description, Room room, User user) {
        this.date = date;
        this.categorie = categorie;
        this.description = description;
        this.room = room;
        this.user = user;
        room.addNewDamage(this);
        user.addNewDamage(this);

    }

    /**
     * Setter and getter for the objects.
     */

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Removes the Damage from the Userlist, Damagelist and the Damagelist. The Damage is deleted by the Garbagecollector himself.
     *
     */

    public void deleteDamage() {

        this.room.removeDamageFromList(this);
        this.user.removeDamageFromList(this);
        DataController.allDamages.remove(DataController.allDamages.indexOf(this));
        DataController.logger.info("Schaden wurde gelöscht.");


    }

    public DamageStatus getStatus() {
        return Status;

    }

    public void setDamageStatus(Damage.DamageStatus Status) {
        this.Status = Status;


    }

    public enum DamageStatus {Done, not_done}



}
