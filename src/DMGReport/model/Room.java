package DMGReport.model;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Malte
 */

/**
 * Class "DMGReport.Room" contains all the information about one room.
 */
public class Room extends DatabaseEntitity {

    private List<Damage> currentDamages = new ArrayList<>();
    private int houseNr;
    private int stage;
    private int roomNr;


    /**
     * Registers the information about the user.
     *
     * @param houseNr, stage, roomNr Parameters to fill in the information.
     */
    public Room(int houseNr, int stage, int roomNr) {
        this.houseNr = houseNr;
        this.stage = stage;
        this.roomNr = roomNr;

    }

    public Room() {

    }

    /**
     * Setter and Getter of the objects.
     */

    public List<Damage> getCurrentDamages() {
        return currentDamages;
    }


    public int getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(int houseNr) {
        this.houseNr = houseNr;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getRoomNr() {
        return roomNr;
    }

    public void setRoomNr(int roomNr) {
        this.roomNr = roomNr;
    }

    /**
     * Add a new damage to list
     * @param newDamage Parameter which defines the new damage to add.
     */

    public void addNewDamage(Damage newDamage) {
        this.currentDamages.add(newDamage);
    }

    /**
     * A method to get specific information about the damage done to one room.
     */

    public void listAllDamagesFromRoom() {
        int i = 1;
        System.out.println("Liste aller Schäden vom Raum");
        System.out.println("************************");
        for (Damage ausgabe : currentDamages) {
            System.out.println(i + ". Schaden");
            System.out.println(ausgabe.getDate());
            System.out.println(ausgabe.getRoom().getHouseNr() + "." + ausgabe.getRoom().getStage() + "." + ausgabe.getRoom().getRoomNr());
            System.out.println(ausgabe.getUser().getFirstName());
            System.out.println(ausgabe.getCategorie());
            System.out.println(ausgabe.getDescription());
            System.out.println("************************");
            i++;
        }
    }

    /**
     * A method to delete one specific damage from the "storage" of damages. It´s done with methods of the "List" class.
     *
     * @param deleteDamage Parameter to define what damage you want deleted.
     */

    public void removeDamageFromList(Damage deleteDamage) {
        this.currentDamages.remove(currentDamages.indexOf(deleteDamage));
    }


}
