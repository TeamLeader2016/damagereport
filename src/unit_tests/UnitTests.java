package unit_tests;

import DMGReport.controller.DataController;
import DMGReport.model.Damage;
import DMGReport.model.Room;
import DMGReport.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

/**
 * Created by Alex on 24.01.2016.
 */

public class UnitTests {

    static Room ROOM;
    static User USER;
    static Damage DAMAGE;
    DataController DC = new DataController();


    public UnitTests() throws ParseException {
    }

    @Before
    public void startUp() throws Exception {
        ROOM = new Room(1, 1, 1);
        USER = new User("Max", "Meier", "M", "01.01.1992", 00000000, "12345", "Geheimfrage", "Geheimantwort", 1000);
        DAMAGE = new Damage(DC.getDate(), "Kategorie", "Beschreibung", ROOM, USER);
    }

    /**
     * User class test
     *
     * @throws Exception
     */
    @Test
    public void testAddNewDamage() throws Exception {
        startUp();
        USER.addNewDamage(DAMAGE);

        Assert.assertEquals(USER.getReportedDamages().size(), 2);
    }

    /**
     * User class test
     *
     * @throws Exception
     */
    @Test
    public void testRemoveDamageFromList() throws Exception {
        startUp();
        USER.removeDamageFromList(DAMAGE);
        System.out.println("Inhalt: " + USER.getReportedDamages().size());
        Assert.assertEquals("CurrentDamages should't contain one damage-object now.", USER.getReportedDamages().size(), 0);
    }

    /**
     * Tests for the Room class
     *
     * @throws Exception
     */
    @Test
    public void testaddNewDamage() throws Exception {
        startUp();
        ROOM.addNewDamage(DAMAGE);


        Assert.assertEquals("Current Damage", ROOM.getCurrentDamages().size(), 2);

    }

    /**
     * Tests for the Damage class
     *
     * @throws Exception
     */
    @Test
    public void testremoveDamageFromList() throws Exception {
        startUp();
        ROOM.removeDamageFromList(DAMAGE);

        Assert.assertEquals("Current Damage", ROOM.getCurrentDamages().size(), 0);

    }

    /**
     * Tests for the User class
     *
     * @throws Exception
     */
    @Test
    public void testFindUser() throws Exception {
        startUp();

        User testUser1 = new User("Vorname1", "Nachname", "Männlich", "01.01.1992", 123120, "passwort", "Geheimfrage", "Geheimantwort", 1000);
        User testUser2 = new User("Vorname2", "Nachname", "Männlich", "01.01.1992", 123121, "passwort", "Geheimfrage", "Geheimantwort", 1000);

        DC.allUsers.add(testUser1);
        DC.allUsers.add(testUser2);
        Assert.assertEquals("User should be found in the list", DC.findUser(123120), testUser1);
        Assert.assertNotEquals("No User with the matricNumber exists", DC.findUser(876523), testUser1);

        DC.allUsers.clear();
        Assert.assertNull("Result should be 'null'", DC.findUser(123123));
    }

    @Test
    public void testLoginUser() throws Exception {
        startUp();
        DC.allUsers.clear();

        User testUser1 = new User("Vorname1", "Nachname", "Männlich", "01.01.1992", 123120, "passwort1", "Geheimfrage", "Geheimantwort", 1000);
        User testUser2 = new User("Vorname2", "Nachname", "Männlich", "01.01.1992", 123121, "passwort2", "Geheimfrage", "Geheimantwort", 1000);

        DC.allUsers.add(testUser1);
        DC.allUsers.add(testUser2);

        DC.loginUser(123120, "passwort1");

        Assert.assertEquals("logged in User should be testUser1", DC.currentUser, testUser1);
        Assert.assertTrue("Return of the function should be true", DC.loginUser(123120, "passwort1"));
        DC.allUsers.clear();
        Assert.assertFalse("Return of the funcion sould be false", DC.loginUser(123120, "passwort1"));
    }


}
